let client = null;

module.exports = function() {
  if(!client) {
    const redis = require('redis');
    const util = require('util');
    
    client = redis.createClient('redis://127.0.0.1:6379');
    client.hget = util.promisify(client.hget);
    client.hmset = util.promisify(client.hmset);
    client.get = util.promisify(client.get);
  }

  return client;
}