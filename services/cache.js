const mongoose = require('mongoose');
const client = require('./redisClient')();
const exec = mongoose.Query.prototype.exec;

//#region Mongoose Query caching

mongoose.Query.prototype.cache = function(options = {}) {
  this.useCache = true;
  this.hashKey = JSON.stringify(options.key || 'default');

  return this;
}


mongoose.Query.prototype.exec = async function() {
  if(!this.useCache) {
    return exec.apply(this, arguments);
  }

  const key = JSON.stringify(Object.assign({}, this.getQuery(), { collection: this.mongooseCollection.name }));

  // See if we have a value for 'key' in redis
  const cacheValue = await client.hget(this.hashKey, key);
  // If we do, return that
  if(cacheValue) {
    const doc = JSON.parse(cacheValue);

    return Array.isArray(doc) 
    ? doc.map(d => this.model(d))
    : new this.model(doc);
  }

  // Otherwise, issue the query and store the results in redis
  
  const result = await exec.apply(this, arguments);

  client.hset(this.hashKey, key, JSON.stringify(result));
  client.expire(this.hashKey, 86400);
  return result;
}

//#endregion


/**
 * 
 * @param {hashKey} Key to delete from Redis. 
 */
function clearHash(hashKey) {
  client.del(JSON.stringify(hashKey));
}

module.exports = {
  clearHash
};